import qbs
import qbs.Probes


Project{
    property stringList stm32f4periphSearchPaths:["~/projects/cpplibs/mcu/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.6.0/",
                                                  "~/projects/cpplibs/mcu/stm32/STM32F4xx_DSP_StdPeriph_Lib_V1.6.1/",
    ]
    property string MCU : "STM32F40_41xxx"
    StaticLibrary{
        Depends{name:"cpp"}

        targetName:{
            var name_base = "stm32f4periph_"
            name_base = name_base + project.MCU
            if(qbs.buildVariant == "debug")
                name_base = name_base + "d"

            return name_base
        }

        property stringList searchPaths:project.stm32f4periphSearchPaths
        property stringList __searchPaths:{
            var res = [];
            var home
            if(qbs.hostOS.contains("linux"))
                home = qbs.getEnv("HOME")
            else
                home = "d:"

            for (var i = 0; i < searchPaths.length; ++i){
                var s = ""
                if (searchPaths[i].startsWith("~")){
                    s = searchPaths[i].replace("~", home)
                }
                else{
                    s = searchPaths[i]
                }
                res.push(s)
            }
            return res
        }

        Probes.PathProbe{
            id:path_of_stm32f4perith
            names:["stm32f4xx.h"]
            pathSuffixes:["", "Libraries/CMSIS/Device/ST/STM32F4xx/Include"]
            platformPaths:{
                var res = []
                res = res.concat(__searchPaths)
                return res
            }
        }

        property string periph_lib_path:{
            var pp = "/NO_WAY"
            if(path_of_stm32f4perith.found){
                pp = path_of_stm32f4perith.path
                var ccc = pp.split("/")
                var rrr = []
                if(ccc.length > 6){
                    for (var i = 0; i < ccc.length - 6; ++i){
                        rrr.push(ccc[i])
                    }
                    pp = rrr.join("/")
                }
            }
            return pp
        }

        Group {
            name: "The Product itself"
            fileTagsFilter: product.type
            qbs.install: true
            qbs.installDir: "lib"
        }

        Group{
            name:"conf"
            files:[
                "conf/stm32f4xx_conf.h",
            ]
        }
        Group{
            name:"CMSIS"
            prefix: {
                var pref = periph_lib_path + "/Libraries/CMSIS/Include/"
                return pref
            }
            files:[
                "arm_common_tables.h",
                "arm_const_structs.h",
                "arm_math.h",
                "core_cm4.h",
                "core_cmFunc.h",
                "core_cmInstr.h",
                "core_cmSimd.h",
            ]
        }
        Group{
            name:"periph"
            prefix: {
                var pref = periph_lib_path + "/Libraries/STM32F4xx_StdPeriph_Driver/"
                return pref
            }
            files:{
                var files = ["inc/*.h"]
                files = files.concat(["src/stm32f4xx_adc.c",
                                      "src/stm32f4xx_crc.c",
                                      "src/stm32f4xx_dbgmcu.c",
                                      "src/stm32f4xx_dma.c",
                                      "src/stm32f4xx_exti.c",
                                      "src/stm32f4xx_flash.c",
                                      "src/stm32f4xx_gpio.c",
                                      "src/stm32f4xx_i2c.c",
                                      "src/stm32f4xx_iwdg.c",
                                      "src/stm32f4xx_pwr.c",
                                      "src/stm32f4xx_rcc.c",
                                      "src/stm32f4xx_rtc.c",
                                      "src/stm32f4xx_sdio.c",
                                      "src/stm32f4xx_spi.c",
                                      "src/stm32f4xx_syscfg.c",
                                      "src/stm32f4xx_tim.c",
                                      "src/stm32f4xx_usart.c",
                                      "src/stm32f4xx_wwdg.c",
                                      "src/misc.c"])
                if(project.MCU == "STM32F40_41xxx") {
                    files = files.concat(["src/stm32f4xx_cryp.c",
                                          "src/stm32f4xx_hash.c",
                                          "src/stm32f4xx_rng.c",
                                          "src/stm32f4xx_can.c",
                                          "src/stm32f4xx_dac.c",
                                          "src/stm32f4xx_dcmi.c",
                                          "src/stm32f4xx_fsmc.c"])
                }

                return files
            }
        }
        Group{
            name:"math"
            prefix: {
                var pref = periph_lib_path + "/Libraries/CMSIS/DSP_Lib/Source/"
                return pref
            }
            files:[
                "BasicMathFunctions/*.c",
                "CommonTables/*.c",
                "ComplexMathFunctions/*.c",
                "ControllerFunctions/*.c",
                "FastMathFunctions/*.c",
                "FilteringFunctions/*.c",
                "MatrixFunctions/*.c",
                "StatisticsFunctions/*.c",
                "SupportFunctions/*.c",
                "TransformFunctions/*.c"
            ]
        }
        Group{
            name:"device"
            prefix: {
                var pref = periph_lib_path + "/Libraries/CMSIS/Device/ST/STM32F4xx/Include/"
                return pref
            }
            files:["*.h"]
        }

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT","STM32F4XX","ARM_MATH_CM4"])
            defs = defs.concat([project.MCU, "__FPU_PRESENT=1"])
            defs = defs.concat(["HSE_VALUE=8000000"])
            return defs
        }
        cpp.includePaths:{
             var paths = base
             paths = paths.concat([periph_lib_path + "/Libraries/CMSIS/Include/",
                                   periph_lib_path + "/Libraries/STM32F4xx_StdPeriph_Driver/inc",
                                   periph_lib_path + "/Libraries/CMSIS/Device/ST/STM32F4xx/Include/",
                                   "conf",
                                  ])
             return paths
         }

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]

        property stringList MCU_FLAGS: ["-mthumb","-mcpu=cortex-m4"]
        property stringList FPU_FLAGS: ["-mfloat-abi=hard","-mfpu=fpv4-sp-d16"]

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat(["-std=gnu11"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            flags = flags.concat(["-specs=nano.specs","-specs=rdimon.specs"])
            return flags
        }
    }
}
